<?php

declare(strict_types=1);

namespace Pamparam83\Telegabot;

use GuzzleHttp\Client;
use Pamparam83\Telegabot\Methods\BaseMethod;
use Pamparam83\Telegabot\Methods\Message\Message;
use Pamparam83\Telegabot\Methods\Webhook;
use ReflectionClass;

/**
 *
 */
final readonly class Telega
{
    private Client $client;

    public function __construct(private string $token)
    {
        $this->client = new Client(['base_uri' => 'https://api.telegram.org/bot' . $this->token.'/']);
    }

    public function webHook()
    {
        return new Webhook($this->token);
    }

    public function send($methodObject): array
    {
        $method = (new ReflectionClass($methodObject))->getShortName();
        $response = $this->client->post( lcfirst($method), [
            'query' => $methodObject->getParams(),
        ]);
        $body = $response->getBody();
        $contents = json_decode($body->getContents(), true, 512, JSON_THROW_ON_ERROR);

        return $contents['result'] ?: [];
    }
}

<?php

declare(strict_types=1);

namespace Pamparam83\Telegabot\Methods\Message;

use Pamparam83\Telegabot\Methods\BaseMethod;

/**
 * @link https://core.telegram.org/bots/api#forcereply
 */
final class InlineKeyboardButton  implements ReplyMarkupInterface
{
    use BaseMethod;

    private array $config;
    public function __construct(
        public string $text,
        public string $callback_data = ''

    ) {
        $this->config['text'] = $this->text;
        $this->config['callback_data'] = $this->callback_data;
    }


}

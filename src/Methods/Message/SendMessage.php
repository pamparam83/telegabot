<?php

declare(strict_types=1);

namespace Pamparam83\Telegabot\Methods\Message;

use Pamparam83\Telegabot\Methods\BaseMethod;

/**
 * @link https://core.telegram.org/bots/api#sendmessage
 *
 * @property string   $parse_mode
 * @property string   $text
 * @property bool     $disable_web_page_preview
 * @property bool     $disable_notification
 * @property int|null $reply_to_message_id
 */
final class SendMessage
{
    use BaseMethod;

    private array $config = [
        'parse_mode' => 'Markdown',
    ];

    public function __construct(public int $chat_id, private readonly string $text)
    {
        $this->config['text'] = $this->text;
        $this->config['chat_id'] = $this->chat_id;
    }

    /**
     * Режим разбора сущностей в сообщении
     */
    public function setParseMode(string $parse_mode): self
    {
        $tg = clone $this;
        $tg->config['parse_mode'] = $parse_mode;
        return $tg;
    }

    /**
     * Отключает предварительный просмотр ссылок в этом сообщении
     */
    public function setDisableWebPagePreview(bool $disable_web_page_preview): self
    {
        $tg = clone $this;
        $tg->config['disable_web_page_preview'] = $disable_web_page_preview;
        return $tg;
    }

    /**
     * Отправляет сообщение молча. Пользователи получат уведомление без звука.
     */
    public function setDisableNotification(bool $disable_notification): self
    {
        $tg = clone $this;
        $tg->config['disable_notification'] = $disable_notification;
        return $tg;
    }

    /**
     * Если сообщение является ответом, нужно указать ID исходного сообщения
     */
    public function setReplyToMessageId(?int $reply_to_message_id): self
    {
        $tg = clone $this;
        $tg->config['reply_to_message_id'] = $reply_to_message_id;
        return $tg;
    }

    /**
     * Дополнительные возможности интерфейса. Сериализованный объект JSON для встроенной клавиатуры,
     * настраиваемой клавиатуры для ответов, инструкций по удалению клавиатуры для ответов или принудительному ответу
     * пользователя.
     */
    public function setReplyMarkup(ReplyMarkupInterface $reply_markup): self
    {
        $tg = clone $this;
        $keyboard = [
            'inline_keyboard' => [
                [$reply_markup->getParams()],
            ],
        ];
        $tg->config['reply_markup'] = json_encode($keyboard);
        return $tg;
    }

}

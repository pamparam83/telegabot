<?php

declare(strict_types=1);

namespace Pamparam83\Telegabot\Methods;

/**
 * @link https://core.telegram.org/bots/api#setwebhook
 */
final readonly class Webhook
{
    public function __construct(private string $token)
    {
    }

    /**
     *  Добавляем хук
     */
    public function add(string $hook): false|string
    {
        return file_get_contents('https://api.telegram.org/bot' . $this->token . '/setWebhook?url=' . $hook);
    }

    /**
     *  Получить информации о хуке
     */
    public function getInfo(): false|string
    {
        return file_get_contents('https://api.telegram.org/bot' . $this->token . '/getWebhookInfo');
    }

    /**
     * удаляем хук
     */
    public function delete(): false|string
    {
        return file_get_contents('https://api.telegram.org/bot' . $this->token . '/deleteWebhook');
    }
}

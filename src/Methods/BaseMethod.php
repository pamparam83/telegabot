<?php

declare(strict_types=1);

namespace Pamparam83\Telegabot\Methods;

/**
 *  Преобразовываем объекты в массив
 */
trait BaseMethod
{
    public function getParams(): array
    {
        return $this->config;
    }
}
